# My dotfiles/configurations for bspwm and i3

Some of the configurations are modified versions of other peoples configurations, tweaked to fit my hardware and needs. Feel free to do the same

![preview](https://gitlab.com/arthurhawron/dotfiles-new/-/raw/main/preview.png)
